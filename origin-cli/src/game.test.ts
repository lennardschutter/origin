import { describe, it } from 'mocha';
import { assert } from 'chai';

import { Game } from './game';
import { Content } from './interface';

describe('Write', () => {
  it('has the right initial state', () => {
    const game = new Game();
    const { health, score, position, rooms } = game.data;
    
    assert.equal(health, 5);
    assert.equal(score, 0);
    assert.equal(position.x, 0);
    assert.equal(position.y, 0);
    assert.equal(rooms.length, 1);
  });

  it('has the right position after moving up', () => {
    const game = new Game();
    game.moveUp();

    const { x, y } = game.data.position;

    assert.equal(x, 0);
    assert.equal(y, -1);
  });

  it('has the right position after moving down', () => {
    const game = new Game();
    game.moveDown();

    const { x, y } = game.data.position;

    assert.equal(x, 0);
    assert.equal(y, 1);
  });

  it('has the right position after moving right', () => {
    const game = new Game();
    game.moveRight();

    const { x, y } = game.data.position;

    assert.equal(x, 1);
    assert.equal(y, 0);
  });

  it('has the right position after moving left', () => {
    const game = new Game();
    game.moveLeft();

    const { x, y } = game.data.position;

    assert.equal(x, -1);
    assert.equal(y, 0);
  });

  it('adds rooms', () => {
    const game = new Game();

    game.addRoom(Content.Food, pos(1, 0));
    game.addRoom(Content.Monster, pos(2, 0));
    game.addRoom(Content.Gold, pos(3, 0));
    game.addRoom(Content.Empty, pos(4, 0));

    const { rooms } = game.data;

    assert.equal(rooms.length, 5);
  });

  it('processes room with food', () => {
    const game = new Game();

    game.addRoom(Content.Food, pos());
    game.processRoom(pos());

    const { score, health } = game.data;

    assert.equal(health, 6);
    assert.equal(score, 0);
  });

  it('processes room with monster', () => {
    const game = new Game();

    game.addRoom(Content.Monster, pos());
    game.processRoom(pos());

    const { score, health } = game.data;

    assert.equal(health, 4);
    assert.equal(score, 0);
  });

  it('processes room with gold', () => {
    const game = new Game();

    game.addRoom(Content.Gold, pos());
    game.processRoom(pos());

    const { score, health } = game.data;

    assert.equal(health, 5);
    assert.equal(score, 1);
  });

  it('gets message for room with food', () => {
    const game = new Game();

    game.addRoom(Content.Food, pos());

    assert.equal(game.getMessage(pos()), "Nom, you've found some food...");
  });

  it('gets message for room with monster', () => {
    const game = new Game();

    game.addRoom(Content.Monster, pos());

    assert.equal(game.getMessage(pos()), "Ouch, there was a monster here...");
  });

  it('gets message for room with gold', () => {
    const game = new Game();

    game.addRoom(Content.Gold, pos());

    assert.equal(game.getMessage(pos()), "Yay, you've uncovered shiny gold...");
  });

  it('gets message for room with nothing', () => {
    const game = new Game();

    game.addRoom(Content.Empty, pos());

    assert.equal(game.getMessage(pos()), "Oh, this room appears to be empty...");
  });

  it('returns wether room has been visited', () => {
    const game = new Game();

    game.addRoom(Content.Empty, pos());

    assert.equal(game.hasBeenToRoom(pos()), true);
  });

  it('returns wether room has not been visited', () => {
    const game = new Game();

    game.addRoom(Content.Empty, pos());

    assert.equal(game.hasBeenToRoom({x: 2, y: 0}), false);
  });
});

const pos = (x = 1, y = 1) => ({x, y});