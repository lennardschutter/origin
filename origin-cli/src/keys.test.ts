import { describe, it } from 'mocha';
import { assert } from 'chai';

import { isUp, isDown, isLeft, isRight, isEscape } from './keys';

describe('Keys', () => {
  it('detects up key', () => {
    assert.equal(isUp({name: 'up'}), true);
    assert.equal(isUp({name: 'return'}), false);
  });

  it('detects down key', () => {
    assert.equal(isDown({name: 'down'}), true);
    assert.equal(isDown({name: 'return'}), false);
  });

  it('detects left key', () => {
    assert.equal(isLeft({name: 'left'}), true);
    assert.equal(isLeft({name: 'return'}), false);
  });

  it('detects right key', () => {
    assert.equal(isRight({name: 'right'}), true);
    assert.equal(isRight({name: 'return'}), false);
  });

  it('detects escape key', () => {
    assert.equal(isEscape({name: 'escape'}), true);
    assert.equal(isEscape({name: 'return'}), false);
  });
});