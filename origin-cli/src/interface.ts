export enum Content {
  Monster = 'MONSTER',
  Food = 'FOOD',
  Gold = 'GOLD',
  Empty = 'EMPTY'
}

export interface Coords {
  x: number;
  y: number;
}

export interface Room {
  position: Coords;
  content: Content;
}

export interface State {
  health: number;
  score: number;
  position: Coords;
  rooms: Room[];
}