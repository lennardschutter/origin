import Axios, { AxiosResponse, AxiosError, AxiosPromise } from 'axios';

import { Coords } from './interface';

const base: string = 'http://localhost:8080';

export const ping = (): AxiosPromise => Axios.get(`${base}/ping`);
export const getRoom = (position: Coords): AxiosPromise => Axios.get(`${base}/room/${position.x}/${position.y}`)
