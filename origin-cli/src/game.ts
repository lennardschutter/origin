import { State, Content, Coords, Room } from './interface';

export class Game {
  /**
   * Data
   */
  private state: State = {
    health: 5,
    score: 0,
    position: {
      x: 0,
      y: 0,
    },
    rooms: [{
      position: {
        x: 0,
        y: 0,
      },
      content: Content.Empty
    }]
  }

  /**
   * Get methods
   */
  public get data() { 
    return this.state;
  }; 
  public get valid() {
    return this.state.health > 0;
  } 

  /**
   * Move functions
   */
  public moveUp() {
    this.updatePosition('y', -1);
  }
  public moveDown() {
    this.updatePosition('y', +1);
  }
  public moveLeft() {
    this.updatePosition('x', -1);
  }
  public moveRight() {
    this.updatePosition('x', +1);
  }
  
  /**
   * Message
   */
  public getMessage(position: Coords) {
    const room = this.getRoom(position);

    switch (room.content) {
      case Content.Food: return "Nom, you've found some food...";
      case Content.Monster: return "Ouch, there was a monster here...";
      case Content.Gold: return "Yay, you've uncovered shiny gold...";
      case Content.Empty: return "Oh, this room appears to be empty...";
    }
  }

  /**
   * Room functions
   */
  public get currentRoom() {
    return this.state.position;
  }
  
  public hasBeenToRoom(position: Coords) {
    return this.state.rooms.some(this.matchRoom(position));
  }

  public processRoom(position: Coords) {
    const room = this.getRoom(position);

    switch (room.content) {
      case Content.Food: this.updateStat('health', +1); break;
      case Content.Monster: this.updateStat('health', -1); break;
      case Content.Gold: this.updateStat('score', +1); break;
    }
  }

  public addRoom(content: Content, position: Coords) {
    this.state = {
      ...this.state,
      rooms: [...this.state.rooms, {
        position: {
          ...position
        },
        content
      }]
    };
  };

  /**
   * Private functions
   */

  private getRoom(position: Coords): Room {
    return this.state.rooms.filter(this.matchRoom(position))[0];
  }

  private matchRoom(position: Coords) {
    return (room: Room) => room.position.x === position.x && room.position.y === position.y;
  }

  private updatePosition(direction: 'x' | 'y', distance: number) {
    this.state = {
      ...this.state,
      position: {
        ...this.state.position,
        [direction]: this.state.position[direction] + distance 
      }
    }
  }

  private updateStat(stat: 'health' | 'score', increment: number) {
    this.state = {
      ...this.state,
      [stat]: this.state[stat] + increment
    };
  }
}

export default new Game();