import chalk from 'chalk';

import { State } from './interface';

export const pad = (value: number): string => value < 10 ? ` ${value}` : `${value}`;

const log = (color: 'white' | 'red' | 'green') => (output: string) => console.log(chalk[color](output));

export const writeInfo = log('white');
export const writeError = log('red');
export const writeSuccess = log('green');

export const constructMessage = (message: string = '', state: State) => `❤️ ${pad(state.health)}  🎖 ${pad(state.score)}    ${message}`;