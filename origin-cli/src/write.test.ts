import { describe, it } from 'mocha';
import { assert } from 'chai';

import { State } from './interface';
import { pad, constructMessage } from './write';

describe('Write', () => {
  it('does pad numbers under 10', () => {
    assert.equal(pad(5), " 5");
    assert.equal(pad(1), " 1");
  });

  it('does not pad numbers over 9', () => {
    assert.equal(pad(14), "14");
    assert.equal(pad(65), "65");
  });

  it('constructs a message', () => {
    const state: State = <State>{
      health: 5,
      score: 0
    }
    const message = constructMessage(
      "Hello world!",
      state
    );

    assert.equal(message, "❤️  5  🎖  0    Hello world!")
  });
});