export interface KeyPressEvent {
  name: string;
  ctrl?: boolean;
}

const isKey = (name: string) => (event: KeyPressEvent): boolean => event.name === name;

export const isEscape = isKey('escape');

export const isUp = isKey('up');
export const isDown = isKey('down');
export const isLeft = isKey('left');
export const isRight = isKey('right');