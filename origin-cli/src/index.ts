import * as keypress from 'keypress';
import chalk from 'chalk';
import { AxiosResponse } from 'axios';

import * as Api from './api';
import { isUp, isDown, isLeft, isRight, isEscape, KeyPressEvent } from './keys';
import { writeSuccess, writeError, writeInfo, constructMessage } from './write';

import { State, Content } from './interface';

import Game from './game';

writeInfo("Initializing Game of Life...");
writeInfo("Checking API...");

Api.ping()
  .then(() => init())
  .catch(() => shutdown(`API is not up! Please make sure ${chalk.underline('origin-api')} is running...`));

const shutdown = (message: string) => {
  writeError(message);
  process.stdin.pause();
}

const init = () => {
  keypress(process.stdin);
  
  writeInfo(`Press any ${chalk.underline('arrow')} key to move around or ${chalk.underline('escape')} to quit...`);
  
  process.stdin.on('keypress', (_, key: KeyPressEvent) => {
    if (isEscape(key)) process.stdin.pause();
    if (key && key.ctrl && key.name == 'c') {
      process.stdin.pause();
    }
    
    if (isUp(key)) Game.moveUp();
    if (isDown(key)) Game.moveDown();
    if (isLeft(key)) Game.moveLeft();
    if (isRight(key)) Game.moveRight();

    if (!Game.hasBeenToRoom(Game.currentRoom)) {
      Api.getRoom(Game.currentRoom)
        .then((response: AxiosResponse<Content>) => {
          Game.addRoom(response.data, Game.currentRoom);
          Game.processRoom(Game.currentRoom);
          
          writeInfo(
            constructMessage(
              Game.getMessage(Game.currentRoom),
              Game.data
            )
          );

          if (!Game.valid) {
            shutdown("Game over...");
          }
        })
        .catch(() => {
          shutdown(`API is not up! Please make sure ${chalk.underline('origin-api')} is running...`)
        });
    } else {
      writeInfo(
        constructMessage(
          "Hmm, this room looks familiar...",
          Game.data
        )
      );
    }
  });
   
  process.stdin.setRawMode(true);
  process.stdin.resume();
}