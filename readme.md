# Origin code challenge
## Game of Life

### Usage instructions
There are to packages in this repository. In order to play the game both will need to be installed and run.

##### Running the API
Navigate to the `origin-api` folder and run `npm install` to install the dependancies. Run `npm start` to start up the API or run `npm test` to run the tests.

##### Running the CLI
Navigate to the `origin-cli` folder and run `npm install` to install the dependancies. Run `npm start` to start up the CLI or run `npm test` to run the tests.

> Please note that if the API is not running when the CLI is started it will shut down automatically. 

##### Playing the game
You can use the `↑`, `↓`, `←` or `→`  key to navigate from room to room. The CLI will update you on your progress. If you run out of life the game will shut down. If you wish to exit the game early you can do so by pressing the `escape` key.

When you enter a room you will either find a monster (lose health), gold (gain score), food (gain health) or nothing.

### Approach
##### Assumptions
* The user will have `node` installed
* The user knows how to use CLI applications
* Game is unique for each instance
* Monsters are suicidal
* Food does not replentish

##### Modifications
* Room with food
* Empty rooms

##### Technologies
* Node
* Express (API)
* Mocha (testing)
* Chai (testing)
* Chalk (log)
* Axios (http requests)
* Keypress (key listeners)
* Typescript (typings)

##### Development
Since this was a code challenge with a rough time limit I started with a straw man prototype so I could get the application up and running quickly. Next I implemented the API and hooked this up. After adding some tests I refactored the application in seperate files and classes.

I think I spent a little longer than intended on the challenge. I'd never built a CLI application in node before so it was a little bit of trial and error finding the packages that helped me achieve the desired outcome. 

### Feedback
I've had a blast doing this project, mainly because writing a CLI application was new to me so I was really keen to learn more and see what was possible. 

I think the only instruction that was unclear was around the API. Wether this had to be implemented or not.