import * as express from 'express';
import {Application, Request, Response} from 'express';
import chalk from 'chalk';

import { randomContent, Content } from './random';

const server: Application = express();
const port = 8080;

server.listen(port, () => {
  console.log(chalk.green(`API is running on port ${chalk.underline(`http://localhost:${port}/`)}...`));
});

server.get('/ping', (request: Request, response: Response) => response.status(200).send("pong"));
server.get('/room/:x/:y', (request: Request, response: Response) => {
  const content = randomContent();
  response.status(200).send(content);
  console.log(chalk.white(`Request for ${request.params.x}, ${request.params.y} responsed with ${chalk.underline(content)}...`))
});