import { describe, it } from 'mocha';
import { assert } from 'chai';

import { randomContent, randomNumber, Content } from './random';

describe('Random', () => {
  it('randomNumber returns random number between 0 and 9', () => {
    const number = randomNumber();

    assert.equal(number >= 0, true);
    assert.equal(number <= 9, true);
  });
  
  it('randomContent returns food', () => {
    assert.equal(randomContent(0), Content.Food);
  });

  it('randomContent returns gold', () => {
    assert.equal(randomContent(1), Content.Gold);
    assert.equal(randomContent(2), Content.Gold);
  });

  it('randomContent returns monster', () => {
    assert.equal(randomContent(3), Content.Monster);
    assert.equal(randomContent(4), Content.Monster);
  });

  it('randomContent returns empty', () => {
    assert.equal(randomContent(5), Content.Empty);
    assert.equal(randomContent(6), Content.Empty);
    assert.equal(randomContent(7), Content.Empty);
    assert.equal(randomContent(8), Content.Empty);
    assert.equal(randomContent(9), Content.Empty);
  });
});