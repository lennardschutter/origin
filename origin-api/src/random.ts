export enum Content {
  Monster = 'MONSTER',
  Food = 'FOOD',
  Gold = 'GOLD',
  Empty = 'EMPTY'
}

export const randomNumber = () => Math.floor(Math.random() * 10);
export const randomContent = (index: number = randomNumber()): Content => {
  switch (index) {
    case 0: return Content.Food;

    case 1: 
    case 2: return Content.Gold;

    case 3: 
    case 4: return Content.Monster;

    default: return Content.Empty;
  }
}; 